// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase, onValue, ref as refS, set, child, get, update, remove, onChildAdded, onChildChanged, onChildRemoved } from
    "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

//storage

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDdXeCHZ7J9duqVsbHFYRFQ4Soq0YyRZKg",
    authDomain: "administradorplay1.firebaseapp.com",
    databaseURL: "https://administradorplay1-default-rtdb.firebaseio.com",
    projectId: "administradorplay1",
    storageBucket: "administradorplay1.appspot.com",
    messagingSenderId: "875053052346",
    appId: "1:875053052346:web:b16bdcc294cdb065482d83"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// Listar Productos
Listarproductos();

function Listarproductos() {
    const section = document.getElementById("sectProductos");

    const dbref = refS(db, 'Productos');

    onValue(dbref, (snapshot) => {
        const productosPorCategoria = {};

        snapshot.forEach(childSnapshot => {
            const data = childSnapshot.val();
            const categoria = data.categoria;

            if (!productosPorCategoria[categoria]) {
                productosPorCategoria[categoria] = [];
            }
            productosPorCategoria[categoria].push(data);
        });

        section.innerHTML = '';

        for (const [categoria, productos] of Object.entries(productosPorCategoria)) {
            section.innerHTML += `<hr><h2>${categoria}</h2>`;

            productos.forEach(producto => {
                section.innerHTML += `<div class='card'>
                    <img id='urlImag' src='${producto.urlImag}' alt='' width='100' height='200'>
                    <p id='marca' class='anta-regurar'>${producto.marca}</p>
                    <p id='descripcion'>${producto.descripcion}</p>
                    <button>Agregar al carrito</button>
                </div>`;
            });
        }
    }, { onlyOnce: true });
}